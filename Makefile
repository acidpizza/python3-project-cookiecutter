# -------------------------------------------------------------------------
# Ensure Makefile recognizes bash syntax rather than the default sh shell
SHELL := /bin/bash
# -------------------------------------------------------------------------
# Automatically print all commands with help description denoted after ## in same line
.DEFAULT_GOAL := help
.PHONY: help init deactivate dev test format lint

help:
	@awk 'BEGIN {FS = ":.*##"; \
	printf "\033[33mUsage:\033[0m\n  make <target>\n\n", "" } \
	/^[a-z0-9A-Z_-]+:.*?##/ { printf "  \033[32m%-20s\033[0m %s\n", $$1, $$2 } \
	/^##/ { printf "\033[33m%s\033[0m\n", substr($$0, 4) } ' $(MAKEFILE_LIST)
# -------------------------------------------------------------------------

## Initialize Project

init: ## Activate virtual environment
	python3 -m venv venv
	@echo "Unable to source from Makefile. Please run: "
	@echo "source venv/bin/activate"

deactivate: ## Deactivate virtual environment
	@echo "Unable to deactivate from Makefile. Please run: "
	@echo "deactivate"

dev: ## Initialize Developer Environment
	@python3 -c $$'import sys\nif sys.prefix == sys.base_prefix:\n\tprint("Need to activate venv first.")\n\tsys.exit(1)\n\n'
	pip install -U pip
	pip install -e .
	pip install -e .[dev]


## Development

test: ## Run pytest
	@pytest

format: ## Format code
	black tests/
	isort --gitignore tests/

lint: ## Lint code
	flake8 tests/
