{#- ------------------------------------------ -#}
{%- if cookiecutter.mode == 'click-main' -%}
from .cli.entrypoint import cli


def main():
    cli()


{# ------------------------------------------ -#}
{%- elif cookiecutter.mode == 'argparse-config' -%}
import logging

from .cli_config import ProjectConfig, cli_get_config
from .sample_pkg.sample_pkg_one import module_one
from .sample_pkg.sample_pkg_two import module_two


def main():
    config: ProjectConfig = cli_get_config()
    logger = logging.getLogger("main")
    logger.debug(f"config is: {config}")

    module_one.func_one()
    module_two.func_two()


{# ------------------------------------------ -#}
{%- elif cookiecutter.mode == 'fastapi' -%}
import uvicorn
from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, query_param: str | None = None):
    return {"item_id": item_id, "query_param": query_param}


def main():
    config = uvicorn.Config(
        "{{ cookiecutter.project_name }}.main:app", host="0.0.0.0", port=8000, log_level="info", reload=True
    )
    server = uvicorn.Server(config)
    server.run()


{# ------------------------------------------ -#}
{%- endif -%}
if __name__ == "__main__":
    main()
