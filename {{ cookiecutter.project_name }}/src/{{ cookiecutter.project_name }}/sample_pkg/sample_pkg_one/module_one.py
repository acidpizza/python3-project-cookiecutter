import logging

from {{ cookiecutter.project_name }}.sample_pkg.sample_pkg_two import module_two

logger = logging.getLogger("module_one")


def func_one() -> None:
    logger.debug("Inside Func One Start!")
    module_two.func_two()
    logger.debug("Inside Func One End!")


def one_plus(input: int) -> int:
    return input + 1
