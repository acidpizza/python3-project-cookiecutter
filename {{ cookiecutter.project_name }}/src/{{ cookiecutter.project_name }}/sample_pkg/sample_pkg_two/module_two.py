import logging

logger = logging.getLogger("module_two")


def func_two() -> None:
    logger.debug("Inside Func Two Start!")
    logger.debug("Inside Func Two End!")
