import logging


def init_logging(log_level_str: str) -> None:
    # Set log level
    if log_level_str == "debug":
        log_level = logging.DEBUG
    elif log_level_str == "info":
        log_level = logging.INFO
    elif log_level_str == "warn":
        log_level = logging.WARN
    elif log_level_str == "error":
        log_level = logging.ERROR
    elif log_level_str == "fatal" or log_level_str == "critical":
        log_level = logging.FATAL
    else:
        log_level = logging.NOTSET

    logging.basicConfig(
        level=log_level,
        format="[%(asctime)s %(name)s %(levelname)s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )
