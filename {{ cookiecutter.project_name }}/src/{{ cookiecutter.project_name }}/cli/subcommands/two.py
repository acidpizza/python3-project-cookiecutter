import click

from {{ cookiecutter.project_name }}.sample_pkg.sample_pkg_two import module_two


@click.command()
def two():
    """
    This runs package two
    """
    module_two.func_two()
