import click

from {{ cookiecutter.project_name }}.sample_pkg.sample_pkg_one import module_one


@click.command(no_args_is_help=True)
@click.argument("INPUT_DIR", type=click.Path(exists=True))
@click.option("-n", "--number", type=int, default=1, help="Number of repetitions")
@click.option(
    "--myvar", type=str, envvar="MYVAR", help="Can also be read from env: MYVAR"
)
@click.option("--input_file", type=click.File("r"), help="Input file to process.")
def one(input_dir, number, myvar, input_file):
    """
    This runs package one

    INPUT_DIR: input directory to process
    """
    module_one.func_one()
