import click

from {{ cookiecutter.project_name }}.logging import init_logging

from .subcommands import one, two

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


@click.group(context_settings=CONTEXT_SETTINGS)
@click.option("-v", "--verbose/--no-verbose", is_flag=True, help="Print verbose")
def cli(verbose):
    """
    This is the program description.
    """
    if verbose:
        click.secho("Verbose enabled", fg="green", bold=True)  # Print colours
        log_level = "debug"
    else:
        click.secho("Verbose disabled", fg="red", bold=True)  # Print colours
        log_level = "info"
    init_logging(log_level)


cli.add_command(one.one)
cli.add_command(two.two)
