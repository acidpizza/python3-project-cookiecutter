import argparse
from dataclasses import dataclass

from .logging import init_logging


@dataclass
class ProjectConfig:
    name: str
    number: int
    verbose: bool


def cli_get_config() -> ProjectConfig:
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=lambda args: parser.print_help())

    # positional arg
    parser.add_argument("name")
    # option flag
    parser.add_argument(
        "-v", "--verbose", help="print verbose logs", action="store_true"
    )
    # option value
    parser.add_argument("--number", help="some number", type=int, default=5)

    args = parser.parse_args()

    if args.verbose:
        log_level = "debug"
    else:
        log_level = "info"
    init_logging(log_level)

    return ProjectConfig(
        name=args.name,
        number=args.number,
        verbose=args.verbose,
    )
