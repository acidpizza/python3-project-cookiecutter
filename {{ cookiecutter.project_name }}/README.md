## {{ cookiecutter.project_name }}

### Initialize Project

```bash
# See available commands
make

# Create venv
make activate
source venv/bin/activate

# Install dependencies
make dev
```

### VSCode Extensions

```yaml
# Language
- Python

# Linter
- Pylance
- Flake8

# Formatter
- Black Formatter  # Auto-format: ctrl+shift+i
- isort            # Quick fix: ctrl+.

# Dev Environment
- DevContainers    # Start container: F1/ctrl+shift+p -> rebuild and reopen in container
                   # Create new terminal: ctrl+shift+`
                   # Open terminal: ctrl+`
```