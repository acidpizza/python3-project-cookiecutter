from {{ cookiecutter.project_name }}.sample_pkg.sample_pkg_one import module_one


def test_one_plus():
    assert module_one.one_plus(5) == 6
    assert module_one.one_plus(0) == 1
    assert module_one.one_plus(-1) == 0
    assert module_one.one_plus(-5) == -4
