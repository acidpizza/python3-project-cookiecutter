import subprocess

from git import Repo


def run_common_tests(bin_path, project_path):
    '''We run commands manually to make use of generated venv'''

    # Init git repo for isort --gitignore
    Repo.init(project_path, bare=False)

    # make dev
    subprocess.check_call([f"{bin_path}/pip", "install", "-U", "pip"], cwd=project_path)
    subprocess.check_call([f"{bin_path}/pip", "install", "-e", "."], cwd=project_path)
    subprocess.check_call([f"{bin_path}/pip", "install", "-e", ".[dev]"], cwd=project_path)

    # make format-check
    subprocess.check_call([f"{bin_path}/black", "--check", "--diff", "src/", "tests/"], cwd=project_path)
    subprocess.check_call([f"{bin_path}/isort", "--check", "--diff", "--gitignore", "src/", "tests/"], cwd=project_path)

    # make lint
    subprocess.check_call([f"{bin_path}/flake8", "src/", "tests/"], cwd=project_path)

    # make test
    subprocess.check_call([f"{bin_path}/pytest"], cwd=project_path)


# ------------------- TESTS ----------------------------------------


def test_mode_click_main(cookies, venv):
    result = cookies.bake(
        extra_context={
            "project_name": "myproject",
            "project_description": "An example python3 project.",
            "mode": "click-main",
        }
    )
    assert result.exit_code == 0
    assert result.exception is None

    bin_path = venv.bin
    project_path = result.project_path
    run_common_tests(bin_path, project_path)

    # Test click apps in click-main
    subprocess.check_call([f"{bin_path}/myproject", "-v", "one", "/tmp"], cwd=project_path)
    subprocess.check_call([f"{bin_path}/myproject", "-v", "two"], cwd=project_path)


def test_mode_argparse_config(cookies, venv):
    result = cookies.bake(
        extra_context={
            "project_name": "myproject",
            "project_description": "An example python3 project.",
            "mode": "argparse-config",
        }
    )
    assert result.exit_code == 0
    assert result.exception is None

    bin_path = venv.bin
    project_path = result.project_path
    run_common_tests(bin_path, project_path)

    # Test click apps in argparse-config
    subprocess.check_call([f"{bin_path}/myproject", "/tmp", "-v"], cwd=project_path)


def test_mode_fastapi(cookies, venv):
    result = cookies.bake(
        extra_context={
            "project_name": "myproject",
            "project_description": "An example python3 project.",
            "mode": "fastapi",
        }
    )
    assert result.exit_code == 0
    assert result.exception is None

    run_common_tests(venv.bin, result.project_path)

    # Not sure how to test fastapi startup
