## Python3 Project Cookie Cutter

Python3 project template.

```bash
cookiecutter <project url>
```

## Parameters

| Field               | Description                      |
| ------------------- | -------------------------------- |
| project_name        | The name for this project        |
| project_description | The description for this project |
| mode                | See below                        |

## Modes

### click-main

Use `click` as main cli.

- `cli/` folder is used to manage click commands, with `entrypoint.py` as the entrypoint.

### argparse-config

Use `argparse` as a config tool.

- `cli_config.py` is used to produce a configuration object which is returned to the main code with all the desired options.

### fastapi

Use fastapi template. WIP.