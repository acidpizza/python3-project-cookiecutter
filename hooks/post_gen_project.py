import os
import shutil


def remove(filepath):
    if os.path.isfile(filepath):
        os.remove(filepath)
    elif os.path.isdir(filepath):
        shutil.rmtree(filepath)


cwd = os.getcwd()  # prints /absolute/path/to/{{cookiecutter.project_name}}
click_main = "{{cookiecutter.mode}}" == "click-main"
click_config = "{{cookiecutter.mode}}" == "argparse-config"
fastapi = "{{cookiecutter.mode}}" == "fastapi"

if click_main:
    remove(f"{cwd}/src/{{cookiecutter.project_name}}/cli_config.py")

elif click_config:
    remove(f"{cwd}/src/{{cookiecutter.project_name}}/cli")

elif fastapi:
    remove(f"{cwd}/src/{{cookiecutter.project_name}}/cli_config.py")
    remove(f"{cwd}/src/{{cookiecutter.project_name}}/cli")
