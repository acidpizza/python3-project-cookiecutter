import re
import sys


MODULE_REGEX = r'^[_a-z][_a-z0-9]+$'

project_name = '{{ cookiecutter.project_name }}'

if not re.match(MODULE_REGEX, project_name):
  print(f'ERROR: Project name [{project_name}] is not a valid Python module name. It should be all small caps with optional underscores, and not start with a number.')
  sys.exit(1)